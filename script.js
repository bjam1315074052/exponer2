//https://developers.google.com/maps/documentation/javascript/adding-a-google-map#maps_add_map-javascript
function iniciarMap(){
    var coord = {lat:-0.9773134090537978, lng:-80.6876123442936 };
    var map = new google.maps.Map(document.getElementById('map'),{
      zoom: 10,
      center: coord
    });
   
    var marker = new google.maps.Marker({
      position: coord,
      map: map
    });
}